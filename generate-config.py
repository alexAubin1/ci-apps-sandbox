#! /usr/bin/python3
# -*- coding: utf-8 -*-

import yaml

d = {
    ".install":
    {
        "stage": "build",
        "image": "debian:latest",
        "script": [
            'echo yunohost app install "$INSTALL_ARGS" > install_$INSTALL_MODE.txt'
        ],
        "artifacts": {
            "paths": ["install_$INSTALL_MODE.txt"]
        }
    },
    "install-root":
    {
        "extends": ".install",
        "variables":
        {
            "INSTALL_MODE": "root",
            "INSTALL_ARGS": "domain=domain.tld&path=/&is_public=1"
        }
    },
    "install-subpath":
    {
        "extends": ".install",
        "variables":
        {
            "INSTALL_MODE": "subpath",
            "INSTALL_ARGS": "domain=domain.tld&path=/subpath&is_public=1"
        }
    }
}

with open("config.yml", "w") as f:
    yaml.safe_dump(d, f, default_flow_style=False)
